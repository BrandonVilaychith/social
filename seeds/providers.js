/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('card_providers').del()
  await knex('card_providers').insert([
    {name: 'Visa'},
    {name: 'Discover'},
    {name: 'Mastercard'},
    {name:'American Express'}
  ]);
};
