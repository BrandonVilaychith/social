
exports.up = function(knex) {
  return knex.schema.createTable('users', table => {
    table.increments('id', { primaryKey: true }).unsigned().notNullable();
    table.string('first_name', 50).notNullable();
    table.string('last_name', 50).notNullable();
    table.string('email', 80).notNullable();
    table.string('password').notNullable();
    table.timestamp('create_at').defaultTo(knex.fn.now())
    table.timestamp('updated_at').nullable()
  })
  .createTable('user_address', table => {
    table.increments('id', { primaryKey: true }).unsigned().notNullable();
    table.string('address_line_1').notNullable();
    table.string('address_line_2').nullable();
    table.string('city', 100);
    table.string('postal_code', 20);
    table.string('country');
    table.string('phone_number', 20)
    table.integer('user_id').references('id').inTable('users')
  })
  .createTable('card_providers', table => {
    table.increments('id', { primaryKey: true }).unsigned().notNullable();
    table.string('name')
  })
  .createTable('user_payment', table => {
    table.increments('id', { primaryKey: true }).unsigned().notNullable();
    table.integer('provider_id').references('id').inTable('card_providers');
    table.string('account_number').notNullable();
    table.date('expiry').notNullable();
  })
};

exports.down = function(knex) {
    return knex.schema
        .dropTable('user_payment')
        .dropTable('card_providers')
        .dropTable('user_address')
        .dropTable('users')
};
